program parser_testing
    use ParserModule
    use ISO_C_BINDING
    implicit none

    TYPE (C_PTR) :: cparser
    character (len=*), parameter :: string = "1+2*x^y"
    character (len=*), parameter :: vars = "x,y"
    DOUBLE PRECISION, dimension(1:2) :: args
    DOUBLE PRECISION :: reslt

    call initParser( cparser ) !works

    call parserAddVariables(cparser, vars) !works

    call parserParse(cparser, string) ! works?

    args(1)=1.43
    args(2)=4

    reslt = parserEvaluate(cparser, args)

    print *, "The value is ", reslt

    call deleteParser(cparser) !works

end program parser_testing
