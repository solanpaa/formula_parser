#include <array>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "formula_parser.h"

/** 
 * @brief Save a two-dimensional array to file
 * @tparam size_x size of the array's first dimension
 * @tparam size_y size of the array's second dimension
 * @param filename Path to the file where the array will be saved
 * @param arr The array to be saved
 */
template <std::size_t size_x, std::size_t size_y>
void vector2d2file(std::string filename, double (&arr)[size_x][size_y]) {
    std::ofstream out(filename.c_str(), std::ios::trunc);
    for(unsigned i=0;i<size_x;i++) {
        for(unsigned j=0;j<size_y;j++) {
            out << arr[i][j] << " ";
        }
        out << std::endl;
    }
    out.close();
}

int main() {

    // Initialize the parser
    formula_parser parser;

    // Define two variables
    parser.add_variables({"x","y"});

    // Define constants
    parser.add_constant("xL", 480);
    parser.add_constant("yL", 580);

    parser.add_constant("offset", 10);
    parser.add_constant("yc", 400);
    parser.add_constant("p", 0.8);
    parser.add_constant("discont", 50);
    parser.add_constant("wy2", 40.876685201774535);
    parser.add_constant("yd", 539.1233147982255);
    parser.add_constant("xd", 399.4459384178116);
    parser.add_constant("yc2", 350);
    parser.add_constant("xc", 342.8652214031487);
    parser.add_constant("xc2", 384.);
    parser.add_constant("kl", -0.8226955719370256);
    parser.add_constant("cl", 671.943450177959);
    parser.add_constant("a", 750.2018588877584);
    parser.add_constant("b", 4254.606163353555);

    // Define the expression to be parsed into a function
    parser.parse("max((y-yd)/wy2, (abs(x)-xd)/(xL-xd))*step(y-yd) + step((abs(x)-(kl*y+cl))/(xL-(kl*y+cl)))*step(y-yc2)*step(yc-y)*(abs(x)-(kl*y+cl))/(xL-(kl*y+cl)) + (abs(x) - a/b*sqrt(y)*sqrt(2*b+y)-offset)/(xL-a/b*sqrt(y)*sqrt(2*b+y)-offset)*step(y-yc)*step((yL-wy2)-y) * step((abs(x) - a/b*sqrt(y)*sqrt(2*b+y)-offset)/(xL-a/b*sqrt(y)*sqrt(2*b+y)-offset))");

    // Parse the function
    const auto fun = parser.get_function();

    // Compute the function output and save to a file
    const unsigned Nx=600;
    const unsigned Ny=600;
    double arr[Nx][Ny];
    double dx = 2*480.0/Nx;
    double dy = 2*580.0/Ny;
    for(unsigned i=0;i<Nx;i+=1) {
        for(unsigned j=0;j<Ny;j+=1){
            arr[i][j]=fun({-480+dx*i,-580+dy*j});
      }
    }

    vector2d2file("tmp", arr);

    return 0;
}

