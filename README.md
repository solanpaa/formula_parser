formula_parser converts an expression stored in a string to reverse polish notation,
and provides functions for evaluating the expression for different argument values.