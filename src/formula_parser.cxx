/*
 * formula_parser.cxx
 *
 * Copyright 2015 Janne Solanpää <janne@solanpaa.fi>
 *
 */

#include "formula_parser.h"

double step(double x){
    if (x > 0) return 1.0;
    if (x < 0) return 0.0;
    else return 0.5;
}

formula_parser::formula_parser(){
    // Add pre-defined operators
    add_operator("-", 1, std::negate<double>(), associativity::left, 0);
    add_operator("-", 2, std::minus<double>(), associativity::left, 1);
    add_operator("+", 2, std::plus<double>(), associativity::left, 1);
    add_operator("*", 2, std::multiplies<double>(), associativity::left, 2);
    add_operator("/", 2, std::divides<double>(), associativity::left, 2);
    add_operator("^", 2, static_cast<double(*)(double, double)>(&std::pow), associativity::right, 2);

    // Add pre-defined functions, static_cast helps to resolve overloads
    typedef double ftype(const double);
    add_function("ln", 1, static_cast<ftype*>(&std::log));
    add_function("log", 1, static_cast<ftype*>(&std::log));
    add_function("log10", 1, static_cast<ftype*>(&std::log10));
    add_function("log2", 1, static_cast<ftype*>(&std::log10));
    add_function("exp2", 1, static_cast<ftype*>(&std::exp2));
    add_function("exp", 1, static_cast<ftype*>(&std::exp));
    add_function("sqrt", 1, static_cast<ftype*>(&std::sqrt));
    add_function("abs", 1, &std::fabs<double>);
    add_function("max", 2, &std::fmax<double, double>);
    add_function("step", 1, &step);

    // Add pre-defined constants
    add_constant("PI", pi);
    add_constant("pi", pi);
    add_constant("Pi", pi);
    add_constant("e", e);
    add_constant("E", e);
    add_constant("golden_ratio", phi);
    add_constant("euler_gamma", euler);

    evaluator =
evaluator = [](std::vector<double> args, std::deque<std::pair<formula_parser::token_type, formula_parser::token>> postfix, unsigned num_variables){
            if(args.size() != num_variables)
                throw std::invalid_argument("Invalid number of arguments.");

            unsigned idx;

            std::vector<constant> tokens;
            tokens.reserve(postfix.size()/2);
            for(const auto& it : postfix){
                switch(std::get<0>(it)){
                case(token_type::constant):
                    tokens.push_back(boost::get<constant>(std::get<1>(it)));
                break;

                case(token_type::unary_operator):
                case(token_type::function_1):
                    if(tokens.size()<1)
                        throw std::invalid_argument("Too few arguments for function/operator");

                    tokens.back() = boost::get<function_1>(std::get<1>(it))(tokens.back());
                break;

                case(token_type::binary_operator):
                case(token_type::function_2):
                    if(tokens.size()<2)
                        throw std::invalid_argument("Too few arguments for function/operator");

                    tokens.end()[-2] = boost::get<function_2>(std::get<1>(it))(tokens.end()[-2], tokens.back());
                    tokens.pop_back();
                break;

                case(token_type::function_3):
                    if(tokens.size()<3)
                        throw std::invalid_argument("Too few arguments for function/operator");

                    tokens.end()[-3] = boost::get<function_3>(std::get<1>(it))(tokens.end()[-3], tokens.end()[-2], tokens.back());
                    tokens.pop_back();
                    tokens.pop_back();
                break;

                case(token_type::function_4):
                    if(tokens.size()<4)
                        throw std::invalid_argument("Too few arguments for function/operator");

                    tokens.end()[-4] = boost::get<function_4>(std::get<1>(it))(tokens.end()[-4], tokens.end()[-3], tokens.end()[-2], tokens.back());
                    tokens.pop_back();
                    tokens.pop_back();
                    tokens.pop_back();
                break;

                case(token_type::function_5):
                    if(tokens.size()<5)
                        throw std::invalid_argument("Too few arguments for function/operator");

                    tokens.end()[-5] = boost::get<function_5>(std::get<1>(it))(tokens.end()[-5], tokens.end()[-4], tokens.end()[-3], tokens.end()[-2], tokens.back());
                    tokens.pop_back();
                    tokens.pop_back();
                    tokens.pop_back();
                    tokens.pop_back();
                break;

                case(token_type::variable):
                    idx = boost::get<variable>(std::get<1>(it));
                    tokens.push_back(args.at(idx-1));
                break;

                case(token_type::parenths_left):
                case(token_type::parenths_right):
                case(token_type::function_argument_separator):
                case(token_type::none):
                break;
                }


            }
            assert(tokens.size()==1);
            return tokens.at(0);
    };

}

void formula_parser::parse(std::string expression){

    // Remove whitespace from expression
    expression.erase(std::remove_if(expression.begin(), expression.end(), [](auto ch){ return std::isspace(ch, std::locale()); }), expression.end());

    // Define stacks for us
    std::stack<std::tuple<token_type, token, int>> opers;

    // Define variables
    token_type rtok_type;
    token rtok;
    int rint;
    associativity rassoc;

    bool next_is_possibly_unary_operator = true;

    while(expression.size() != 0){
        std::tie(rtok_type, rtok, rint, rassoc) = get_next_token(expression, next_is_possibly_unary_operator);

        switch(rtok_type){
            case(token_type::constant):
            case(token_type::variable):
                postfix.push_back(std::make_pair(rtok_type, rtok));
                next_is_possibly_unary_operator = false;
            break;

            case(token_type::function_1):
            case(token_type::function_2):
            case(token_type::function_3):
            case(token_type::function_4):
            case(token_type::function_5):
                opers.push(std::make_tuple(rtok_type, rtok, rint));
                next_is_possibly_unary_operator = false;
            break;

            case(token_type::function_argument_separator):
                while(!opers.empty() and std::get<0>(opers.top() ) != token_type::parenths_left){
                    postfix.push_back(std::make_pair(std::get<0>(opers.top()), std::get<1>(opers.top()))); // Move here?
                    opers.pop();
                }

                if( opers.empty() ) throw std::ios::failure("Missing parentheses or misplaced comma");

                next_is_possibly_unary_operator = true;
            break;

            case(token_type::unary_operator):
            case(token_type::binary_operator):
                if (rassoc == associativity::left) {
                    while(!opers.empty() and (rint <= std::get<2>( opers.top() )) ){
                        postfix.push_back(std::make_pair(std::get<0>(opers.top()), std::get<1>(opers.top())));
                        opers.pop();
                    }
                } else {
                    while(!opers.empty() and (rint < std::get<2>( opers.top() )) ){
                        postfix.push_back(std::make_pair(std::get<0>(opers.top()), std::get<1>(opers.top())));
                        opers.pop();
                    }
                }
                opers.push(std::make_tuple(rtok_type, rtok, rint));
                next_is_possibly_unary_operator = true;
            break;

            case(token_type::parenths_left):
                opers.push(std::make_tuple(rtok_type, rtok, rint));
                next_is_possibly_unary_operator = true;
            break;

            case(token_type::parenths_right):
                while(!opers.empty() and (std::get<0>(opers.top()) != token_type::parenths_left) ){
                    postfix.push_back(std::make_pair(std::get<0>(opers.top()), std::get<1>(opers.top())));
                    opers.pop();
                }
                if( !opers.empty() ) {
                    if(std::get<0>(opers.top()) == token_type::parenths_left)
                        opers.pop();
                    else throw std::ios::failure("Missing parentheses or misplaced comma");

                } else {
                    throw std::ios::failure("Missing parentheses or misplaced comma");
                }
                next_is_possibly_unary_operator = false;
            break;

            case(token_type::none):
                throw std::ios::failure("Was not passed a token");
            break;
        }

    }

    while(!opers.empty()){
        if(std::get<0>(opers.top()) == token_type::parenths_left or std::get<0>(opers.top()) == token_type::parenths_right)
            throw std::ios::failure("Mismatched parentheses");

        postfix.push_back(std::make_pair(std::get<0>(opers.top()), std::get<1>(opers.top())));
        opers.pop();
    }
}

double formula_parser::evaluate(std::vector<double> args) const {
    if(args.size() != variables.size())
        throw std::invalid_argument("Invalid number of arguments.");

    unsigned idx;

    std::vector<constant> tokens;
    tokens.reserve(postfix.size()/2);
    for(const auto& it : postfix){
        switch(std::get<0>(it)){
        case(token_type::constant):
            tokens.push_back(boost::get<constant>(std::get<1>(it)));
        break;

        case(token_type::unary_operator):
        case(token_type::function_1):
            if(tokens.size()<1)
                throw std::invalid_argument("Too few arguments for function/operator");

            tokens.back() = boost::get<function_1>(std::get<1>(it))(tokens.back());
        break;

        case(token_type::binary_operator):
        case(token_type::function_2):
            if(tokens.size()<2)
                throw std::invalid_argument("Too few arguments for function/operator");

            tokens.end()[-2] = boost::get<function_2>(std::get<1>(it))(tokens.end()[-2], tokens.back());
            tokens.pop_back();
        break;

        case(token_type::function_3):
            if(tokens.size()<3)
                throw std::invalid_argument("Too few arguments for function/operator");

            tokens.end()[-3] = boost::get<function_3>(std::get<1>(it))(tokens.end()[-3], tokens.end()[-2], tokens.back());
            tokens.pop_back();
            tokens.pop_back();
        break;

        case(token_type::function_4):
            if(tokens.size()<4)
                throw std::invalid_argument("Too few arguments for function/operator");

            tokens.end()[-4] = boost::get<function_4>(std::get<1>(it))(tokens.end()[-4], tokens.end()[-3], tokens.end()[-2], tokens.back());
            tokens.pop_back();
            tokens.pop_back();
            tokens.pop_back();
        break;

        case(token_type::function_5):
            if(tokens.size()<5)
                throw std::invalid_argument("Too few arguments for function/operator");

            tokens.end()[-5] = boost::get<function_5>(std::get<1>(it))(tokens.end()[-5], tokens.end()[-4], tokens.end()[-3], tokens.end()[-2], tokens.back());
            tokens.pop_back();
            tokens.pop_back();
            tokens.pop_back();
            tokens.pop_back();
        break;

        case(token_type::variable):
            idx = boost::get<variable>(std::get<1>(it));
            tokens.push_back(args.at(idx-1));
        break;

        case(token_type::parenths_left):
        case(token_type::parenths_right):
        case(token_type::function_argument_separator):
        case(token_type::none):
        break;
        }


    }
    assert(tokens.size()==1);
    return tokens.at(0);
}


void formula_parser::add_function(std::string expression, size_t num_arguments, function_type fun){
    try {
        is_name_already_defined(expression);
    } catch(...) {
        throw;
    }

    token_type ttype;
    switch(num_arguments){
        case 1:
            ttype = token_type::function_1;
        break;

        case 2:
            ttype = token_type::function_2;
        break;

        case 3:
            ttype = token_type::function_3;
        break;

        case 4:
            ttype = token_type::function_4;
        break;

        case 5:
            ttype = token_type::function_5;
        break;

        default:
            // TODO: Throw something
        break;

    }
    functions.emplace_back(std::make_tuple(expression, 10, ttype, fun, associativity::left));
}

void formula_parser::add_operator(std::string expression, size_t num_arguments, function_type fun, associativity assoc, unsigned eval_order){

    token_type ttype;
    switch(num_arguments){
        case 1:
            ttype = token_type::unary_operator;
        break;

        case 2:
            ttype = token_type::binary_operator;
        break;

        default:
            // TODO: Throw something
        break;

    }
    operators.emplace_back(std::make_tuple(expression, eval_order, ttype, fun, assoc));
}

void formula_parser::add_constant(std::string expression, double value){
    try {
        is_name_already_defined(expression);
    } catch(...) {
        throw;
    }

    constants.push_back(std::make_pair(expression, value));
}

void formula_parser::add_variables(std::vector<std::string> variable_names){
    // Check for name conflicts
    for(const auto& name : variable_names){
        try {
            is_name_already_defined(name);
        } catch(...) {
            throw;
        }
        variables.push_back(std::make_pair(name, variables.size()+1));
    }


}

void formula_parser::is_name_already_defined(std::string expression){
    auto it = std::find_if(operators.begin(), operators.end(), [&expression](const auto& it){ return expression == std::get<0>(it);});
    if (it!=operators.end()) throw std::invalid_argument("An operator already exists with that name.");

    auto it2 = std::find_if(functions.begin(), functions.end(), [&expression](const auto& it){ return expression == std::get<0>(it);});
    if (it2!=functions.end()) throw std::invalid_argument("A function already exists with that name.");

    auto it3 = std::find_if(constants.begin(), constants.end(), [&expression](const auto& it){ return expression == std::get<0>(it);});
    if (it3!=constants.end()) throw std::invalid_argument("A constant already exists with that name.");
}

std::tuple<bool, formula_parser::token_type, formula_parser::token, int, formula_parser::associativity>  formula_parser::is_a_token(std::string str, bool possibly_unary){
    // Check parentheses
    if ( str == "(" ) return std::make_tuple(true, token_type::parenths_left, parenths::left, -1, associativity::left);
    if ( str == ")" ) return std::make_tuple(true, token_type::parenths_right, parenths::right, -1, associativity::left);

    // Check comma, i.e., function argument separator
    if( str== "," ) return std::make_tuple(true, token_type::function_argument_separator, ",", 0, associativity::left);

    // Check numbers

    size_t tmp = 0;
    try {
        double value = std::stod(str, &tmp);
        if(tmp == str.size()) return std::make_tuple(true, token_type::constant, value, 0, associativity::left);
    }
    catch(...){ /* Do nothing, just continue */}

    // Check variables
    const auto itv = std::find_if( variables.begin(), variables.end(), [&str](const auto& i) {
        return str == std::get<0>(i);
    });

    if( itv != variables.end() ) return std::make_tuple(true, token_type::variable, std::get<1>(*itv), 0, associativity::left);

    // Check math constants
    const auto itc = std::find_if( std::begin(constants), std::end(constants),
            [&str](const constant_list_item& i){
                return str == std::get<0>(i);
            }
        );

    if( itc != std::end(constants) ) return std::make_tuple(true, token_type::constant, std::get<1>(*itc), 0, associativity::left);

    // Check all operators if there's a match

    // Handle "-"-operators separately
    if( str == "-" ) {
        if(possibly_unary)  // Then "-" is negate
            return std::make_tuple(true, token_type::unary_operator,
                                  std::get<3>(operators[0]), std::get<1>(operators[0]), std::get<4>(operators[0]));
        else // Then "-" is minus
            return std::make_tuple(true, token_type::binary_operator,
                                  std::get<3>(operators[1]), std::get<1>(operators[1]), std::get<4>(operators[0]));
    }

    const auto it = std::find_if( std::begin(operators)+2, std::end(operators),
            [&str](const auto& i){
                return str == std::get<0>(i);
             }
         );

    if ( it != std::end(operators) ) return std::make_tuple(true, std::get<2>(*it), std::get<3>(*it), std::get<1>(*it), std::get<4>(*it));

    const auto it2 = std::find_if( std::begin(functions), std::end(functions),
            [&str](const auto& i){
                return str == std::get<0>(i);
             }
         );

    if ( it2 != std::end(functions) ) return std::make_tuple(true, std::get<2>(*it2), std::get<3>(*it2), std::get<1>(*it2), std::get<4>(*it2));


    // If there's no match, return
    return std::make_tuple(false, token_type::none, 0u, 0, associativity::left);
}

std::tuple<formula_parser::token_type, formula_parser::token, int, formula_parser::associativity> formula_parser::get_next_token(std::string& str, bool possibly_unary_operator){
    assert(str.size() != 0);

    if(possibly_unary_operator){

        // Check here first for a possible negative number
        bool is_a_negative_number = false;
        try{
            size_t tmp;
            std::stod(str.substr(0,2), &tmp);
            is_a_negative_number = true;
        }catch(...){}
        if ( str.substr(0,1)=="-" and !is_a_negative_number){
            str = str.substr(1, str.size() );
            return std::make_tuple(token_type::unary_operator, std::get<3>(operators[0]), std::get<1>(operators[0]), std::get<4>(operators[0]));
        }
    } else {
        if (str.substr(0,1)=="+"){
            str = str.substr(1,str.size());
            return std::make_tuple(token_type::binary_operator, std::get<3>(operators[2]), std::get<1>(operators[2]), std::get<4>(operators[2]));
        }
        if (str.substr(0,1) == "-") {
            str = str.substr(1, str.size());
            return std::make_tuple(token_type::binary_operator, std::get<3>(operators[1]), std::get<1>(operators[1]), std::get<4>(operators[1]));
        }
    }



    std::tuple<bool, token_type, token, int, associativity> tmp;
    std::tuple<token_type, token, int, associativity> res;
    size_t i_found;
    bool found_a_token=false;

    for(size_t i = 1; i<= str.size(); i++){
        tmp = is_a_token(str.substr(0,i), possibly_unary_operator);
        if(std::get<0>(tmp)){
            res = std::make_tuple(std::get<1>(tmp), std::get<2>(tmp), std::get<3>(tmp), std::get<4>(tmp));
            i_found = i;
            found_a_token=true;
        }
    }
    if(found_a_token){
        str = str.substr(i_found);
        return res;
    }
    else{
        std::string err("String not empty, but no more tokens could be extracted. ");
        err.append(str);
        throw std::logic_error(err);
    }
}
