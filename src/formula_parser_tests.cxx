#include "formula_parser.h"
#include <iostream>
#include "gtest/gtest.h"
#include <type_traits>

class formula_parser_tests : public ::testing::Test {
protected:
    void test_plus() {
        auto res = parser.is_a_token("+");

        EXPECT_TRUE(std::get<0>(res));

        EXPECT_EQ(formula_parser::token_type::binary_operator, std::get<1>(res));
        EXPECT_FALSE(formula_parser::token_type::unary_operator == std::get<1>(res));

        auto oper = boost::get<formula_parser::function_2>(std::get<2>(res));

        EXPECT_EQ(3, oper(1,2));
    }

    void test_minus(){
        auto parser = formula_parser();
        auto res = parser.is_a_token("-");

        EXPECT_TRUE(std::get<0>(res));

        EXPECT_EQ(formula_parser::token_type::binary_operator, std::get<1>(res));
        EXPECT_FALSE(formula_parser::token_type::unary_operator == std::get<1>(res));

        auto oper = boost::get<formula_parser::function_2>(std::get<2>(res));

        EXPECT_EQ(-1, oper(1,2));
    }

    void test_unary_minus(){
        auto parser = formula_parser();
        auto res = parser.is_a_token("-", true);

        EXPECT_TRUE(std::get<0>(res));

        EXPECT_EQ(formula_parser::token_type::unary_operator, std::get<1>(res));
        EXPECT_FALSE(formula_parser::token_type::binary_operator == std::get<1>(res));

        auto oper = boost::get<formula_parser::function_1>(std::get<2>(res));

        EXPECT_EQ(-2, oper(2));
    }

    void test_divide(){
        auto parser = formula_parser();
        auto res = parser.is_a_token("/");

        EXPECT_TRUE(std::get<0>(res));

        EXPECT_EQ(formula_parser::token_type::binary_operator, std::get<1>(res));
        EXPECT_FALSE(formula_parser::token_type::unary_operator == std::get<1>(res));

        auto oper = boost::get<formula_parser::function_2>(std::get<2>(res));

        EXPECT_EQ(0.5, oper(1,2));
    }

    void test_multiply(){
        auto parser = formula_parser();
        auto res = parser.is_a_token("*");

        EXPECT_TRUE(std::get<0>(res));

        EXPECT_EQ(formula_parser::token_type::binary_operator, std::get<1>(res));
        EXPECT_FALSE(formula_parser::token_type::unary_operator == std::get<1>(res));

        auto oper = boost::get<formula_parser::function_2>(std::get<2>(res));

        EXPECT_EQ(4, oper(2,2));
    }

    void test_log(){
        auto parser = formula_parser();
        auto res = parser.is_a_token("log");

        EXPECT_TRUE(std::get<0>(res));

        EXPECT_EQ(formula_parser::token_type::function_1, std::get<1>(res));
        EXPECT_FALSE(formula_parser::token_type::binary_operator == std::get<1>(res));

        auto oper = boost::get<formula_parser::function_1>(std::get<2>(res));

        EXPECT_EQ(0, oper(1));
    }
private:
    formula_parser parser;
};

TEST_F(formula_parser_tests, plus){ test_plus(); }
TEST_F(formula_parser_tests, minus){ test_minus(); }
TEST_F(formula_parser_tests, unary_minus){ test_unary_minus(); }
TEST_F(formula_parser_tests, divide){ test_divide(); }
TEST_F(formula_parser_tests, multiply){ test_multiply(); }
TEST_F(formula_parser_tests, log){ test_log(); }


/*


TEST(token_parser, left_parenthesis){
    auto res = is_a_token("(");

    EXPECT_TRUE(std::get<0>(res));

    EXPECT_EQ(token_type::parenths_left, std::get<1>(res));
    EXPECT_FALSE(token_type::parenths_right == std::get<1>(res));

    auto oper = boost::get<parenths>(std::get<2>(res));

    EXPECT_EQ(parenths::left, oper);
}

TEST(token_parser, right_parenthesis){
    auto res = is_a_token(")");

    EXPECT_TRUE(std::get<0>(res));

    EXPECT_EQ(token_type::parenths_right, std::get<1>(res));
    EXPECT_FALSE(token_type::parenths_left == std::get<1>(res));

    auto oper = boost::get<parenths>(std::get<2>(res));

    EXPECT_EQ(parenths::right, oper);
}

TEST(token_parser, comma){
    auto res = is_a_token(",");

    EXPECT_TRUE(std::get<0>(res));

    EXPECT_EQ(token_type::function_argument_separator, std::get<1>(res));
    EXPECT_FALSE(token_type::parenths_left == std::get<1>(res));

    auto oper = boost::get<farg_separator>(std::get<2>(res));

}

TEST(token_parser, PI) {
    auto res = is_a_token("PI");

    EXPECT_TRUE(std::get<0>(res));

    EXPECT_EQ(token_type::constant, std::get<1>(res));

    auto value = boost::get<constant>(std::get<2>(res));

    EXPECT_EQ(M_PI, value);
}

TEST(token_parser, e) {
    auto res = is_a_token("e");

    EXPECT_TRUE(std::get<0>(res));

    EXPECT_EQ(token_type::constant, std::get<1>(res));

    auto value = boost::get<constant>(std::get<2>(res));

    EXPECT_EQ(M_E, value);
}

TEST(token_parser, not_a_token1){
    EXPECT_FALSE(std::get<0>(is_a_token("asdoi")));
}

TEST(token_parser, not_a_token2){
    EXPECT_FALSE(std::get<0>(is_a_token("-asdoi")));
}

TEST(token_parser, number1){
    auto res = is_a_token("1");

    EXPECT_TRUE(std::get<0>(res));
    EXPECT_EQ(token_type::constant, std::get<1>(res));

    auto value = boost::get<constant>(std::get<2>(res));

    EXPECT_EQ(1.0, value);
}

TEST(token_parser, number2){
    auto res = is_a_token("-1");

    EXPECT_TRUE(std::get<0>(res));
    EXPECT_EQ(token_type::constant, std::get<1>(res));

    auto value = boost::get<constant>(std::get<2>(res));

    EXPECT_EQ(-1.0, value);
}

TEST(token_parser, number3){
    auto res = is_a_token("-1.2e1");

    EXPECT_TRUE(std::get<0>(res));
    EXPECT_EQ(token_type::constant, std::get<1>(res));

    auto value = boost::get<constant>(std::get<2>(res));

    EXPECT_EQ(-12.0, value);
}


TEST(extract_next_token, test1){
    std::string expression = "1";
    auto res = get_next_token(expression, false);

    EXPECT_EQ(token_type::constant, std::get<0>(res));

    auto value = boost::get<constant>(std::get<1>(res));

    EXPECT_EQ(1.0, value);
}


TEST(extract_next_token, test2){
    std::string expression = "pi+2534*log(13*pi)";
    auto res = get_next_token(expression, false);

    EXPECT_EQ(token_type::constant, std::get<0>(res));

    auto value = boost::get<constant>(std::get<1>(res));

    EXPECT_EQ(M_PI, value);
}

TEST(extract_next_token, test3){ //Should parse 1, but fail if we would try to get the next one too
    std::string expression = "1p+2534*log(13*pi)";
    auto res = get_next_token(expression, false);

    EXPECT_EQ(token_type::constant, std::get<0>(res));

    auto value = boost::get<constant>(std::get<1>(res));

    EXPECT_EQ(1, value);
}


TEST(extract_next_token, test5_end){
    std::string expression = "1";
    auto res = get_next_token(expression, false);

    EXPECT_EQ(token_type::constant, std::get<0>(res));

    auto value = boost::get<constant>(std::get<1>(res));

    EXPECT_EQ(1, value);

}

TEST(extract_next_token, test5_end2){
    std::string expression = "pi";
    auto res = get_next_token(expression, false);

    EXPECT_EQ(token_type::constant, std::get<0>(res));

    auto value = boost::get<constant>(std::get<1>(res));

    EXPECT_EQ(M_PI, value);

}

TEST(extract_next_token, remember_to_extract_not_peek1){
    std::string expression = "1p+2534*log(13*pi)";
    auto res = get_next_token(expression, false);

    EXPECT_EQ(token_type::constant, std::get<0>(res));

    auto value = boost::get<constant>(std::get<1>(res));

    EXPECT_EQ(1, value);
    EXPECT_EQ("p+2534*log(13*pi)", expression);
}

TEST(extract_next_token, remember_to_extract_not_peek2){
    std::string expression = "pi+2534*log(13*pi)";
    auto res = get_next_token(expression, false);

    EXPECT_EQ(token_type::constant, std::get<0>(res));

    auto value = boost::get<constant>(std::get<1>(res));

    EXPECT_EQ(M_PI, value);
    EXPECT_EQ("+2534*log(13*pi)", expression);
}



TEST(extract_next_token, comma){
    std::string expression = ",134+";
    auto res = get_next_token(expression, false);

    EXPECT_EQ(token_type::function_argument_separator, std::get<0>(res));

    auto value = boost::get<farg_separator>(std::get<1>(res));

    EXPECT_EQ(",", value);
    EXPECT_EQ("134+", expression);
}

TEST(extract_next_token, plus_2_previous_is_number){
    std::string expression = "+2";
    auto res = get_next_token(expression, false);

    EXPECT_EQ(token_type::binary_operator, std::get<0>(res));

    auto value = boost::get<binary_oper>(std::get<1>(res));

    EXPECT_EQ("2", expression);
}

TEST(extract_next_token, right_parenthesis){
    std::string expression= ")";
    auto res = get_next_token(expression, false);

    EXPECT_EQ(token_type::parenths_right, std::get<0>(res));
}

TEST(extract_next_token, 2rp){
    std::string expression= "2)";
    auto res = get_next_token(expression, false);

    EXPECT_EQ(token_type::constant, std::get<0>(res));
}

TEST(extract_next_token, function1){
    std::string expression = "ln(1)";

    auto res = get_next_token(expression, true);

    EXPECT_EQ(token_type::function1, std::get<0>(res));

}

TEST(extract_next_token, function2){
    std::string expression = "log(1)";

    auto res = get_next_token(expression, true);

    EXPECT_EQ(token_type::function1, std::get<0>(res));

}

TEST(extract_next_token, function3){
    std::string expression = "exp(1)";

    auto res = get_next_token(expression, true);

    EXPECT_EQ(token_type::function1, std::get<0>(res));

}

// http://stackoverflow.com/questions/11421432/how-can-i-output-the-value-of-an-enum-class-in-c11
template <typename Enumeration>
auto as_integer(Enumeration const value)
    -> typename std::underlying_type<Enumeration>::type
{
    return static_cast<typename std::underlying_type<Enumeration>::type>(value);
}

TEST(shunting_yard_parse, simple){
    std::string expression = "1+2";

    auto result = shunting_yard_parse(expression);

    ASSERT_EQ(3, result.size());


    EXPECT_EQ(1, as_integer(std::get<0>(result.back())));
    result.pop_back();
    EXPECT_EQ(5, as_integer(std::get<0>(result.back())));
    result.pop_back();
    EXPECT_EQ(5, as_integer(std::get<0>(result.back())));

}

TEST(shunting_yard_parse, simple2){
    std::string expression = "1+2-3";

    auto result = shunting_yard_parse(expression);

    ASSERT_EQ(5, result.size());


    EXPECT_EQ(1, as_integer(std::get<0>(result.back())));
    result.pop_back();
    EXPECT_EQ(5, as_integer(std::get<0>(result.back())));
    result.pop_back();
    EXPECT_EQ(1, as_integer(std::get<0>(result.back())));
    result.pop_back();
    EXPECT_EQ(5, as_integer(std::get<0>(result.back())));
    result.pop_back();
    EXPECT_EQ(5, as_integer(std::get<0>(result.back())));
    result.pop_back();
}


TEST(shunting_yard_parse, simple3){
    std::string expression = "1+2--3";

    auto result = shunting_yard_parse(expression);

    ASSERT_EQ(5, result.size());

    EXPECT_EQ(1, as_integer(std::get<0>(result.back())));
    result.pop_back();
    EXPECT_EQ(5, as_integer(std::get<0>(result.back())));
    result.pop_back();
    EXPECT_EQ(1, as_integer(std::get<0>(result.back())));
    result.pop_back();
    EXPECT_EQ(5, as_integer(std::get<0>(result.back())));
    result.pop_back();
    EXPECT_EQ(5, as_integer(std::get<0>(result.back())));
    result.pop_back();
}

TEST(shunting_yard_parse, parenths_1){
    std::string expression = "2*(1+2)";

    auto result = shunting_yard_parse(expression);

    ASSERT_EQ(5, result.size());

    EXPECT_EQ(1, as_integer(std::get<0>(result.back())));
    result.pop_back();
    EXPECT_EQ(1, as_integer(std::get<0>(result.back())));
    result.pop_back();
    EXPECT_EQ(5, as_integer(std::get<0>(result.back())));
    result.pop_back();
    EXPECT_EQ(5, as_integer(std::get<0>(result.back())));
    result.pop_back();
    EXPECT_EQ(5, as_integer(std::get<0>(result.back())));
    result.pop_back();
}

TEST(shunting_yard_evaluate, simple1){
    std::string expression = "1+2-3";
    class
    auto inpout = shunting_yard_parse(expression);

    auto value = shunting_yard_evaluate(inpout);
    EXPECT_EQ(0, value);
}
TEST(shunting_yard_evaluate, simple2){
    std::string expression = "1+2+3";

    auto inpout = shunting_yard_parse(expression);

    auto value = shunting_yard_evaluate(inpout);
    EXPECT_EQ(6, value);
}

TEST(shunting_yard_evaluate, simple3){
    std::string expression = "-1+2+3";

    auto inpout = shunting_yard_parse(expression);

    auto value = shunting_yard_evaluate(inpout);
    EXPECT_EQ(4, value);
}


TEST(shunting_yard_evaluate, simple4){
    std::string expression = "-1+(2)+3";

    auto inpout = shunting_yard_parse(expression);

    auto value = shunting_yard_evaluate(inpout);
    EXPECT_EQ(4, value);
}

TEST(shunting_yard_evaluate, simple5){
    std::string expression = "-1+(-2)+3";

    auto inpout = shunting_yard_parse(expression);

    auto value = shunting_yard_evaluate(inpout);
    EXPECT_EQ(0, value);
}

TEST(shunting_yard_evaluate, simple6){
    std::string expression = "-1+-2+3";

    auto inpout = shunting_yard_parse(expression);

    auto value = shunting_yard_evaluate(inpout);
    EXPECT_EQ(0, value);
}

TEST(shunting_yard_evaluate, func1){
    std::string expression = "-1";

    auto inpout = shunting_yard_parse(expression);

    auto value = shunting_yard_evaluate(inpout);
    EXPECT_EQ(-1, value);
}

TEST(shunting_yard_evaluate, func2){
    std::string expression = "exp(1)";

    auto inpout = shunting_yard_parse(expression);

    auto value = shunting_yard_evaluate(inpout);
    EXPECT_EQ(M_E, value);
}

TEST(shunting_yard_evaluate, func3){
    std::string expression = "log(1)";

    auto inpout = shunting_yard_parse(expression);

    auto value = shunting_yard_evaluate(inpout);
    EXPECT_EQ(0, value);
}

TEST(shunting_yard_evaluate, func4){
    std::string expression = "-log(-2+3*6*exp(-1))";

    auto inpout = shunting_yard_parse(expression);

    auto value = shunting_yard_evaluate(inpout);
    EXPECT_TRUE(fabs(-1.530790717 - value) < 1e-6);
}

TEST(shunting_yard_evaluate, func_and_division){
    std::string expression = "log(1)+6/3";

    auto inpout = shunting_yard_parse(expression);

    auto value = shunting_yard_evaluate(inpout);
    EXPECT_EQ(2, value);
}

TEST(shunting_yard_evaluate, expr1){
    std::string expression = "log(1/15)+6/(3*exp(2)-1)";

    auto inpout = shunting_yard_parse(expression);

    auto value = shunting_yard_evaluate(inpout);
    EXPECT_TRUE(fabs(-2.4245923518579469 - value) < 1e-6);
}

TEST(shunting_yard_evaluate, expr2){
    std::string expression = "1/9*log(1/15+1)+6/(3*exp(2)-1)";

    auto inpout = shunting_yard_parse(expression);

    auto value = shunting_yard_evaluate(inpout);
    EXPECT_TRUE(fabs(0.29062879603732666 - value) < 1e-6);
}

TEST(shunting_yard_evaluate, expr3){
    std::string expression = "2^3";

    auto inpout = shunting_yard_parse(expression);

    auto value = shunting_yard_evaluate(inpout);
    EXPECT_EQ(8, value);
}

TEST(shunting_yard_evaluate, expr4){
    std::string expression = "3*2^3";

    auto inpout = shunting_yard_parse(expression);

    auto value = shunting_yard_evaluate(inpout);
    EXPECT_EQ(24, value);
}

TEST(shunting_yard_evaluate, expr5){
    std::string expression = "log(4/pi)*4^exp(-pi/e)";

    auto inpout = shunting_yard_parse(expression);

    auto value = shunting_yard_evaluate(inpout);
    EXPECT_TRUE(fabs(0.37374783761546421 - value)<1e-6);
}

*/
int main(int argc, char** argv){
    testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
