#ifdef __cplusplus
extern "C"
{
#endif

struct Cformula_parser;
typedef struct Cformula_parser Cformula_parser;

struct parsed_function;
typedef struct parsed_function parsed_function;

void formula_parser_allocate(Cformula_parser**);

void formula_parser_free(Cformula_parser *);

void formula_parser_add_variables(Cformula_parser *, char* vars);

void formula_parser_parse(Cformula_parser * cparser, char* expression);

void formula_parser_evaluate(Cformula_parser* cparser, double* vars, double* output);

void parsed_function_free(parsed_function* cfun);

#ifdef __cplusplus
}
#endif
