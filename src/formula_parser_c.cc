#include "formula_parser_c.h"
#include "formula_parser.h"

extern "C" {

    void formula_parser_allocate(Cformula_parser** cparser ) {
        *cparser = reinterpret_cast<Cformula_parser*>( new formula_parser() );
    }

    void formula_parser_free(Cformula_parser * cparser) {
        delete reinterpret_cast<formula_parser*>(cparser);
    }

    void formula_parser_add_variables(Cformula_parser * cparser, char* vars) {
        std::string str(vars);
        std::remove_if(str.begin(), str.end(), [](auto ch){ return std::isspace(ch, std::locale()); }); // remove whitespace
        std::vector<std::string> variables;

        std::string delimiter=",";

        auto i = str.find(delimiter);
        size_t i2=str.size();

        while(i!=i2){
            i2=i;
            variables.push_back(str.substr(0, i));
            str.erase(0,i+delimiter.length());
            i=str.find(delimiter);
        }

        reinterpret_cast<formula_parser*>(cparser)->add_variables(variables);
    }

    void formula_parser_parse(Cformula_parser * cparser, char* expression) {
        reinterpret_cast<formula_parser*>(cparser)->parse(expression);
    }

    void formula_parser_evaluate(Cformula_parser* cparser, double* vars, double* output) {
        *output = reinterpret_cast<formula_parser*>(cparser)->evaluate(std::vector<double>(vars, vars+reinterpret_cast<formula_parser*>(cparser)->get_num_variables()));
    }

    void formula_parser_get_function(Cformula_parser* cparser, parsed_function* cfun) {
         auto* fun = new std::function<double(std::vector<double>)>(reinterpret_cast<formula_parser*>(cparser)->get_function());
         cfun = reinterpret_cast<parsed_function*>(fun);
    }

    void parsed_function_free(parsed_function* cfun){
        delete reinterpret_cast<std::function<double(std::vector<double>)>*>(cfun);
    }

}
