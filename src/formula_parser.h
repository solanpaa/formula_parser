/*
 * formula_parser.hpp
 *
 * Copyright 2015 Janne Solanpää <janne@solanpaa.fi>
 *
 */
#ifndef _FORMULA_PARSER_HPP_
#define _FORMULA_PARSER_HPP_

#include <iostream>
#include <string>
#include <functional>
#include <vector>
#include <stack>
#include <utility>
#include <tuple>
#include <locale>
#include <algorithm>
#include <stdexcept>
#include <cmath>
#include <type_traits>
#include <boost/variant.hpp>
#include <boost/math/constants/constants.hpp>
#define FRIEND_TEST(test_case_name, test_name)\
friend class test_case_name##_##test_name##_Test

using namespace boost::math::double_constants;

/*
 * Class formula_parser can be used to
 * parse scalar expressions and
 * (1) output the value, or
 * (2) output the function (as std::function)
 * The class implements the Shunting-Yard algorithm.
 */
class formula_parser {
public:
    enum class associativity {
        left,
        right
    };

private:
    friend class formula_parser_tests;
    enum class token_type {
        binary_operator,
        unary_operator,
        function_1, // 1 argument
        function_2, // 2 arguments
        function_3, // 3 arguments
        function_4, // 4 arguments
        function_5, // 5 arguments
        constant, // Including numbers and pre-defined numeric constants
        variable,
        parenths_left,
        parenths_right,
        function_argument_separator, // comma
        none, // no token
    };

    enum class parenths : char {
        left = '(',
        right = ')'
    };

    using function_1 = std::function<double(double)>;
    using function_2 = std::function<double(double, double)>;
    using function_3 = std::function<double(double, double, double)>;
    using function_4 = std::function<double(double, double, double, double)>;
    using function_5 = std::function<double(double, double, double, double, double)>;
    using constant = double;
    using variable = unsigned; // Number indicating the argument place (1st, 2nd, etc.)
    using farg_separator = std::string;
    using token = boost::variant<function_1, function_2, function_3, function_4, function_5, constant, variable, farg_separator, parenths>;
    using function_type = boost::variant<function_1, function_2, function_3, function_4, function_5>;

    // string matching the function/operator, evaluation order, type of token, the actual function, associativity of the operator (left for functions)
    using function_list_item = std::tuple<std::string, unsigned, token_type, function_type, associativity>;

    using constant_list_item = std::pair<std::string, constant>;

    std::tuple<token_type, token, int, associativity> get_next_token(std::string& str, bool possibly_unary_operator);

    // Check if a given string is a token, and if it is, then which kind
    // Last int in return is for order of operator evaluations
    std::tuple<bool, token_type, token, int, associativity>  is_a_token(std::string str, bool possibly_unary=false);

    std::deque<std::pair<token_type, token>> postfix;

    void is_name_already_defined(std::string expression);

    // List of operators
    std::vector<function_list_item> operators;

    // List of functions
    std::vector<function_list_item> functions;

    // List of constants
    std::vector<constant_list_item> constants;

    // List of variables
    std::vector<std::pair<std::string, unsigned>> variables;

    std::function<double(std::vector<double>, std::deque<std::pair<token_type, token>>, unsigned)> evaluator;

public:

    formula_parser();
    ~formula_parser(){}

    void parse(std::string expression);

    // Evaluate
    double evaluate(std::vector<double> args) const;

    void add_variables(std::vector<std::string> variable_names);

    void add_function(std::string expression, size_t num_arguments, function_type fun);

    void add_operator(std::string expression, size_t num_arguments, function_type fun, associativity assoc = associativity::left, unsigned eval_order = 10);

    void add_constant(std::string expression, double value);

    std::function<double(std::vector<double>)> get_function(){
        return std::bind(evaluator, std::placeholders::_1, postfix, variables.size());
    }

    void print(){
        std::cout << "Hello, this class works" << std::endl;
    }

    unsigned get_num_variables(){
        return variables.size();
    }

};


#endif // _FORMULA_PARSER_HPP_
