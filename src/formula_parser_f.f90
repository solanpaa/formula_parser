MODULE ParserModule
USE ISO_C_BINDING
implicit none

PUBLIC :: initParser, deleteParser, parserAddVariables, parserParse, parserEvaluate, deleteParsedFunction

INTEGER, parameter :: LEN_EXPRESSION = 2000

INTERFACE

SUBROUTINE formula_parser_allocate(ptr) bind(C)
    USE ISO_C_BINDING
    TYPE (C_PTR), intent(in) :: ptr
END SUBROUTINE formula_parser_allocate

SUBROUTINE formula_parser_free(cparser) bind(C)
    USE ISO_C_BINDING
    TYPE (C_PTR), intent(in), value :: cparser
END SUBROUTINE formula_parser_free

SUBROUTINE parsed_function_free(cparsed_function) bind(C)
    USE ISO_C_BINDING
    TYPE (C_PTR), intent(in), value :: cparsed_function
END SUBROUTINE parsed_function_free

SUBROUTINE formula_parser_add_variables(cparser, variables) bind(C)
    use ISO_C_BINDING
    CHARACTER (kind=c_char), intent(in) :: variables(*)
    TYPE (C_PTR), intent(in), value :: cparser
END SUBROUTINE formula_parser_add_variables

SUBROUTINE formula_parser_parse(cparser, expression) bind(C)
    use ISO_C_BINDING
    CHARACTER (kind=c_char), intent(in) :: expression(*)
    TYPE (C_PTR), intent(in), value :: cparser
END SUBROUTINE formula_parser_parse


SUBROUTINE formula_parser_evaluate(cparser, args, resulting_value) bind(C)
    USE ISO_C_BINDING
    DOUBLE PRECISION, dimension(*), intent(in) :: args
    TYPE (C_PTR), intent(in), value :: cparser
    DOUBLE PRECISION, intent(out) :: resulting_value
END SUBROUTINE formula_parser_evaluate
END INTERFACE

CONTAINS


SUBROUTINE initParser(ptr)
    USE ISO_C_BINDING
    TYPE (C_PTR), intent(out) :: ptr
    call formula_parser_allocate(ptr)
END SUBROUTINE initParser


SUBROUTINE deleteParser(cparser)
    TYPE (C_PTR), intent(in), value :: cparser
    CALL formula_parser_free(cparser)
END SUBROUTINE deleteParser


SUBROUTINE deleteParsedFunction(cparsed_function)
    TYPE (C_PTR), intent(in), value :: cparsed_function
    CALL parsed_function_free(cparsed_function)
END SUBROUTINE deleteParsedFunction


SUBROUTINE parserAddVariables(cparser, variables)
    CHARACTER (LEN=*) :: variables
    TYPE (C_PTR), intent(in) :: cparser
    CHARACTER (LEN=LEN(variables)+1):: cvars
    cvars = variables//CHAR(0)
    CALL formula_parser_add_variables(cparser, cvars)
END SUBROUTINE parserAddVariables


SUBROUTINE parserParse(cparser, expression)
    CHARACTER (LEN=*) :: expression
    TYPE (C_PTR), intent(in), value :: cparser
    CHARACTER (LEN=LEN(expression)+1):: cexpr
    cexpr = expression//CHAR(0)
    CALL formula_parser_parse(cparser, cexpr)
END SUBROUTINE parserParse


FUNCTION parserEvaluate(cparser, args) result(reslt)
    DOUBLE PRECISION, intent(in) :: args(*)
    DOUBLE PRECISION :: reslt
    TYPE (C_PTR), intent(in), value :: cparser
    call formula_parser_evaluate(cparser, args, reslt)
END FUNCTION parserEvaluate


END MODULE ParserModule
